---
title: "Diwali of 2020"
date: 2020-11-15T19:43:10+05:30
description: This years diwali was kinda different for my state (Rajasthan).
---

![Diwal](/diwali_2020.jpg)

<h4>Happy Diwali</h4>

<p>I can say one the best Diwali in years! Why?</p>
<p>Well our state govt imposed a ban on crackers sale, due to corona virus pandemic. So it was a bliss to have silence and a peaceful diwali after so many years.</p>

<p>Though in past i too was an avid lover of crackers, but honestly i have stopped bursting crackers from past few years. I mean, just a noise of something bursting isn't that great to hear after all. And my policy of no crackers wasn't helping either in terms of environment etc. So this ban did have some impact on noise and air pollution this year.</p>

<p>But, I truly trust people of my nation, we will be back with full impact or maybe double impact next year!!!</p>